![Icon](images/icon.png)

DigitizedDecay

This is a web based rewrite of https://gitlab.com/j-romchain/decaydisplayv2, to ensure greater compatibility and accessibility.

This is a small project written to make the world better, with hopes of coordination of https://phet.colorodo.edu to host it.

for now, there is a gitlab pages demo at: https://j-romchain.gitlab.io/DigitizedDecay

this is a app/library for manipulating atoms.

To use:
currently the only way to access it is to download or clone the repository and open DigitizedDecay.html as a local web page in the web browser (double clicking it).

Disclaimer: the accuracy of any information in the app is not guaranteed.

Licence: don't get me in trouble for anything, and don't remove included credits back to the original author (me) with the exception that you may copy or utilize up to 2 functions worth or 50 lines of code, whichever is greater, and remove credits within that code. You MAY reposition credits, such as to include them in a reasonably findable credits or such page in the app. otherwise, you are free to redistribute modify and add whatever you want. this is all under the condition that any legal trouble I may get into as a result of your actions may be deferred upon you and any resulting expense to myself may also be deferred to you should any legal "trouble" occur.

Screenshots:
![Usage example 1](images/usageExample1.png)
![Usage example 2](images/usageExample2.png)