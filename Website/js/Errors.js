// **********************************************************************************
// Title: DecayDisplay (Error Class)
// Author: (Your Name Here)
// Description: a class for reporting errors in JavaScript
// **********************************************************************************

class Error {
    static error(errorMessage) {
        if (Display) {
            this.GUIPopupError(errorMessage);
        } else {
            this.consolePrintlnError(errorMessage);
        }
    }

    static GUIPopupError(errorMessage) {
        // Check if Alert is available (library dependent)
        if (typeof Alert === 'function') {
            const error = new Alert("error", errorMessage);
            error.show();
        } else {
            console.warn("Error: GUI popup not available, falling back to console.");
            this.consolePrintlnError(errorMessage);
        }
    }

    static consolePrintlnError(errorMessage) {
        console.error("ERROR: " + errorMessage);
    }

    static bothError(errorMessage) {
        this.consolePrintlnError(errorMessage);
        this.GUIPopupError(errorMessage);
    }

    static argumentCompleteError() {
        this.bothError(
            "FATAL ERROR: unexpected argument pattern over command line."
        );
    }

    static argumentPartialError() {
        this.bothError(
            "ERROR: bad arguments were given... this has been handled by ignoring the argument and substituting a default."
        );
    }

    static negativeParticleError() {
        this.error(
            "ERROR: something is trying to either make a negative count of of protons or electrons or neutrons, or remove all protons and neutrons from the nucleus... refusing to have negative count, or have an empty nucleus."
        );
    }
    
}
