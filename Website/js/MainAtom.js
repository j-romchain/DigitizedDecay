const MainAtom = {
    protonCount: 1,
    neutronCount: 1,
    electronCount: 1,
    
    setMainAtom(protons, neutrons, electrons) {
        this.setProtons(protons);
        this.setNeutrons(neutrons);
        this.setElectrons(electrons);
    },

    setMainAtomByAtomicNumber(atomicNumber) {
        this.setMainAtom(atomicNumber, PeriodicTable.mostLikelyNeutronCount(atomicNumber), atomicNumber);
    },

    toSave() {
        return `loadSave{protons=`+this.getProtons()+`, neutrons=`+this.getNeutrons()+`, electrons=`+this.getElectrons()+`}`;
    },
    loadSave(saveText) {
        let text = saveText.substring(saveText.indexOf("protons=") + 8);
        this.setProtons(parseInt(text.substring(0, text.indexOf(","))));
        text = text.substring(text.indexOf("neutrons=") + 9);
        this.setNeutrons(parseInt(text.substring(0, text.indexOf(","))));
        text = text.substring(text.indexOf("electrons=") + 10);
        this.setElectrons(parseInt(text.substring(0, text.indexOf("}"))));  
    },
    toString() {
        return "Atom{\n"+
            "Protons="+this.getProtons()+",\n"+
            "Neutrons="+this.getNeutrons()+",\n"+
            "Electrons="+this.getElectrons()+",\n"+
            "Charge="+this.getCharge()+",\n"+
            "AtomicNumber="+this.getAtomicNumber()+",\n"+
            "AtomicMass="+this.getAtomicMass()+",\n"+
            "AtomicWeight="+this.getAtomicWeight()+",\n"+
            "NeutronCountBasedOnProtons="+PeriodicTable.mostLikelyNeutronCount(this.getProtons())+"}";
    },

    getCharge() {
        return this.protonCount - this.electronCount;
    },

    getAtomicNumber() {
        return this.getProtons();
    },

    getAtomicMass() {
        return this.getProtons() + this.getNeutrons();
    },

    getProtons() {
        return this.protonCount;
    },

    setProtons(protons) {
        if (protons < 0||(protons<1&&this.neutronCount<1)) {
            Error.negativeParticleError();
            return;
        }
        this.protonCount = Math.max(0, protons);
    },

    addProtons(protonsToAdd) {
        this.setProtons(this.getProtons() + protonsToAdd);
    },

    getNeutrons() {
        return this.neutronCount;
    },

    setNeutrons(neutrons) {
        if (neutrons < 0||(neutrons<1&&this.protonCount<1)) {
            Error.negativeParticleError();
            return;
        }
        this.neutronCount = Math.max(0, neutrons);
    },

    addNeutrons(neutronsToAdd) {
        this.setNeutrons(this.getNeutrons() + neutronsToAdd);
    },

    getElectrons() {
        return this.electronCount;
    },

    setElectrons(electrons) {
        if (electrons < 0) {
            Error.negativeParticleError();
            return;
        }
        this.electronCount = Math.max(0, electrons);
    },

    addElectrons(electronsToAdd) {
        this.setElectrons(this.getElectrons() + electronsToAdd);
    },

    getAtomicWeight() {
        return (this.protonCount * 1.00727647) + (this.neutronCount * 1.008665) + (this.electronCount * 0.0005489);
    },

    getAtomicWeightRounded() {
        return Math.round(this.getAtomicWeight()*10000000)/10000000;
    },

    radiation(type, emitting) {
        switch (type) {
            case 1:
                if (emitting) {
                    this.emitProton();
                } else {
                    this.absorbProton();
                }
                break;
            case 2:
                if (emitting) {
                    this.emitNeutron();
                } else {
                    this.absorbNeutron();
                }
                break;
            case 3:
                if (emitting) {
                    this.emitElectron();
                } else {
                    this.absorbElectron();
                }
                break;
            case 4:
                if (emitting) {
                    this.emitAlpha();
                } else {
                    this.unEmitAlpha();
                }
                break;
            case 5:
                if (emitting) {
                    this.emitBeta();
                } else {
                    this.unEmitBeta();
                }
                break;
            case 6:
                if (emitting) {
                    this.emitGamma();
                } else {
                    this.unEmitGamma();
                }
                break;
            default:
                console.error("Invalid radiation type:", type);
        }
    },

    unEmitAlpha() {
        this.addProtons(2);
        this.addNeutrons(2);
    },

    emitAlpha() {
        this.addProtons(-2);
        this.addNeutrons(-2);
    },

    unEmitBeta() {
        this.addProtons(-1);
        this.addNeutrons(1);
    },

    emitBeta() {
        this.addProtons(1);
        this.addNeutrons(-1);
    },

    unEmitGamma() {
        // No change in particle count for gamma radiation
    },

    emitGamma() {
        // No change in particle count for gamma radiation
    },

    absorbProton() {
        this.addProtons(1);
    },

    emitProton() {
        this.addProtons(-1);
    },

    absorbNeutron() {
        this.addNeutrons(1);
    },

    emitNeutron() {
        this.addNeutrons(-1);
    },

    absorbElectron() {
        this.addElectrons(1);
    },

    emitElectron() {
        this.addElectrons(-1);
    },
};