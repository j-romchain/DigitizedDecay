class Radiation {
    type="";
    particlePane;
    constructor(type, emitting) {
        var typeNum=ParticleGeneration.signToNumber(type);
        this.particlePane = document.createElementNS("http://www.w3.org/2000/svg","g");
        switch (typeNum) {
            case 1:
                //Proton
                var circle = ParticleGeneration.genProton();
                circle.setAttribute("cx","0")
                circle.setAttribute("cy","0")
                this.particlePane.appendChild(circle);
                break;
            case 2:
                //Neutron
                var circle = ParticleGeneration.genNeutron();
                circle.setAttribute("cx","0")
                circle.setAttribute("cy","0")
                this.particlePane.appendChild(circle);
                break;
            case 3:
            case 5:
                //Beta/Electron
                var circle = ParticleGeneration.genElectron();
                circle.setAttribute("cx","0")
                circle.setAttribute("cy","0")
                this.particlePane.appendChild(circle);
                break;
            case 4:
                //Alpha
                var proton1 = ParticleGeneration.genProton();
                this.particlePane.appendChild(proton1);
                ParticleGeneration.hadronRadius.bind({
                        setValue:function(value) {
                            proton1.setAttribute("cx",value+"");
                            proton1.setAttribute("cy",value+"");
                        }
                    }
                )

                var neutron1 = ParticleGeneration.genNeutron();
                this.particlePane.appendChild(neutron1);
                ParticleGeneration.hadronRadius.bind({
                        setValue:function(value) {
                            neutron1.setAttribute("cx",-value+"");
                            neutron1.setAttribute("cy",value+"");
                            // neutron1.setAttribute("cy",(50+value)+"%")
                        }
                    }
                )

                var proton2 = ParticleGeneration.genProton();
                this.particlePane.appendChild(proton2);
                ParticleGeneration.hadronRadius.bind({
                        setValue:function(value) {
                            proton2.setAttribute("cx",-value+"");
                            proton2.setAttribute("cy",-value+"");
                        }
                    }
                )
                var neutron2 = ParticleGeneration.genNeutron();
                this.particlePane.appendChild(neutron2);
                ParticleGeneration.hadronRadius.bind({
                        setValue:function(value) {
                            neutron2.setAttribute("cx",value+"");
                            neutron2.setAttribute("cy",-value+"");
                        }
                    }
                )
                Display.rotateGroup(this.particlePane, Math.random()*360);
                break;
            case 6:
                //Gamma
                //todo
                function genLightning() {
                    var pane = document.createElementNS("http://www.w3.org/2000/svg","g");
                    var width = new Bindable(0);
                    var height = new Bindable(0); 
                    ParticleGeneration.hadronRadius.bind({
                        setValue:function(value) {
                            width.setValue(value*2);
                            height.setValue(value/2);
                        }
                    })
                    var bottom = document.createElementNS("http://www.w3.org/2000/svg","line")
                    bottom.setAttribute("stroke","black")
                    pane.appendChild(bottom);
                    width.bind({
                        setValue:function(value) {
                            bottom.setAttribute("x1",value*1);
                            bottom.setAttribute("x2",value*0.4);
                        }
                    })
                    height.bind({
                        setValue:function(value) {
                            bottom.setAttribute("y1",value*0.5);
                            bottom.setAttribute("y2",value*0);
                        }
                    })
                    var middle = document.createElementNS("http://www.w3.org/2000/svg","line")
                    middle.setAttribute("stroke","black")
                    pane.appendChild(middle);
                    width.bind({
                        setValue:function(value) {
                            middle.setAttribute("x1",value*0.6);
                            middle.setAttribute("x2",value*0.4);
                        }
                    })
                    height.bind({
                        setValue:function(value) {
                            middle.setAttribute("y1",value*1);
                            middle.setAttribute("y2",value*0);
                        }
                    })
                    var top = document.createElementNS("http://www.w3.org/2000/svg","line")
                    top.setAttribute("stroke","black")
                    pane.appendChild(top);
                    width.bind({
                        setValue:function(value) {
                            top.setAttribute("x1",value*0);
                            top.setAttribute("x2",value*0.6);
                        }
                    })
                    height.bind({
                        setValue:function(value) {
                            top.setAttribute("y1",value*0.5);
                            top.setAttribute("y2",value*1);
                        }
                    })
                    return pane;
                }
                var lightningBolt1 = genLightning();
                var lightningBolt2 = genLightning();
                var lightningBolt3 = genLightning();
                this.particlePane = document.createElementNS("http://www.w3.org/2000/svg","g");
                this.particlePane.appendChild(lightningBolt1);
                this.particlePane.appendChild(lightningBolt2);
                this.particlePane.appendChild(lightningBolt3);
                ParticleGeneration.hadronRadius.bind({
                    setValue:function(value) {
                        Display.translateGroup(lightningBolt1,-value,value/2);
                        Display.translateGroup(lightningBolt3,-value,-value/2);
                    }
                })
                break;
        }
        this.type=type;
        this.animateParticle(this.particlePane,emitting,typeNum);
    };
    animateParticle(particle,emitting,typeNum) {
        class EmmisionAndAbsorbsionAnimation {
            particleGroup;
            rotationGroup;
            animationGroup;
            duration;
            direction;
            startTime;
            currentX = 0;
            currentY = 0;
            emitting;
            onclose;
            delSelfOnClose;
            constructor(particleGroup,duration,direction,emitting,onclose,delSelfOnClose) {
                this.rotationGroup = document.createElementNS("http://www.w3.org/2000/svg","g");
                this.animationGroup = document.createElementNS("http://www.w3.org/2000/svg","g");
                this.particleGroup=particleGroup;
                this.duration=duration;
                this.startTime=performance.now();
                this.direction=direction;
                this.emitting=emitting;
                this.onClose=onclose;
                this.delSelfOnClose=delSelfOnClose;
                this.rotationGroup.appendChild(particleGroup);//plug it into me.
                Display.rotateGroup(this.rotationGroup,direction);
                this.animationGroup.appendChild(this.rotationGroup);//plug it into me.
                // console.log("EmmisionAndAbsorbsionAnimation: emmiting:"+emitting+" direction:"+direction+" onclose:"+onclose);
                // console.log("xToTravel:"+this.getXDistanceINeedToElapse()+" yToTravel:"+this.getYDistanceINeedToElapse());
                // console.log("side:"+(this.isOnHorizontalSides()?"horizontal":"verticle"));//+" yToTravel:"+this.getYDistanceINeedToElapse());
                requestAnimationFrame(() => this.updateFrameToNow())
            }
            updateFrameToNow() {
                this.ensurePose(this.getXPoseIShouldBeAt(),this.getYPoseIShouldBeAt());
                if (this.getPercentageOfTime()<1) {
                    requestAnimationFrame(() => this.updateFrameToNow())
                } else {
                    onClose.apply();
                    if (this.delSelfOnClose) {
                        this.animationGroup.remove();
                    }
                }
            }
            ensurePose(x,y) {
                this.animationGroup.setAttribute("transform","translate("+x+" "+y+")")
            }
            getTimePast() {
                return performance.now()-this.startTime;
            }
            getPercentageOfTime() {
                return this.getTimePast()/this.duration;
            }
            getXPoseIShouldBeAt() {
                return !this.emitting ? this.getXDistanceINeedToElapse()-this.getXDistanceIShouldHaveElapsed():this.getXDistanceIShouldHaveElapsed();
            }
            getYPoseIShouldBeAt() {
                return !this.emitting ? this.getYDistanceINeedToElapse()-this.getYDistanceIShouldHaveElapsed():this.getYDistanceIShouldHaveElapsed();
            }
            getXDistanceIShouldHaveElapsed() {
                return this.getPercentageOfTime()*this.getXDistanceINeedToElapse()
            }
            getYDistanceIShouldHaveElapsed() {
                return this.getPercentageOfTime()*this.getYDistanceINeedToElapse()
            }
            isOnHorizontalSides() {
                return (45<(this.direction%180))&&((this.direction%180)<(90+45));
            }
            isOnVerticleSides() {
                return !this.isOnHorizontalSides();
            }
            isNegativeX() {
                return (270>this.direction) && (this.direction>90);
            }
            isNegativeY() {
                return this.direction>180;
            }
            getDirectionDifferenceFromNearest90() {
                var offset45 = this.direction+45;
                var constrainedToTopRight = offset45%90;// now 90 and 0 in this are really at 45s 
                var unOffset = constrainedToTopRight-45;//now 0 is 90's, and +/- 45 are at 45s
                return Math.abs(unOffset);//now 0 is all 90s, and 45 is all 45s.
            }
            getDistanceOnSide() {
                return Math.tan(this.getDirectionDifferenceFromNearest90()*Math.PI/180) * 50;
            }
            getXDistanceINeedToElapse() {
                if (this.isOnVerticleSides()) {//verticle sides means it goes all the way left/right
                    return (this.isNegativeX()? -1:1) * 50;
                }
                return (this.isNegativeX()? -1:1) * this.getDistanceOnSide();
            }
            getYDistanceINeedToElapse() {
                if (this.isOnHorizontalSides()) {
                    return (this.isNegativeY()? -1:1) * 50;
                }
                return (this.isNegativeY()? -1:1) * this.getDistanceOnSide();
            }
        }
        MainAtom.radiation(typeNum,emitting);
        Display.updateInfo();
        if (emitting) {
            DisplayedAtom.radiation(typeNum,emitting);
        }
        var direction = Math.random()*360;
        var duration = 1000+Math.random()*6000;
        var onClose = function() {
            if (!emitting) {
                DisplayedAtom.radiation(typeNum,emitting);
            }
        }
        var emittedParticle = new EmmisionAndAbsorbsionAnimation(particle,duration,direction,emitting,onclose,true);
        Display.show(emittedParticle.animationGroup);

    }
};
