SaveLoad = {
    load: function (file) {
    
        if (!file) {
            console.error("No file selected!");
            return;
        }
    
        const reader = new FileReader();
    
        reader.onload = function (event) {
            const textContent = event.target.result;
            MainAtom.loadSave(textContent);
            DisplayedAtom.copyMainAtom();
        };
    
        reader.readAsText(file);
    },
    save:function() {
        this.fakeDownloadTextBasedFile("element.ddelement",MainAtom.toSave());
    },
    fakeDownloadTextBasedFile:function(fileName,fileContents) {
        blob = new Blob([fileContents], { type: 'text/plain' });
        link = document.createElement('a');
        link.href = URL.createObjectURL(blob);
        link.download = fileName;
        link.click();

        // Important: Release the temporary URL after the click, it seems the element was never actually on the document.
        // link.parentElement.removeChild(link);
        URL.revokeObjectURL(link.href);   
    }
}