class Bindable {
    toUpdate=[];
    value;
    constructor(initialValue) {
        this.value=initialValue;
    }
    setValue(value) {
        this.value=value;
        this.toUpdate.forEach((bound) => bound.setValue(this.value));
    }
    bind(bindable) {
        this.toUpdate.push(bindable);
        bindable.setValue(this.value);
    }
}
Display = {
    rotateGroup:function(group,angle) {
        group.setAttribute("transform","rotate("+angle+")")
    },
    rotateGroupAboutPoint:function(group, rotateX,rotateY,angle) {
        group.setAttribute("transform","rotate("+angle+" "+rotateX+" "+rotateY+")")
    },
    translateGroup:function(group,xOffset,yOffset) {
        group.setAttribute("transform","translate("+xOffset+" "+yOffset+")")
    },
    updateInfo:function() {
        var atomicNumber = MainAtom.getAtomicNumber();
        document.getElementById("nameBox").innerHTML=PeriodicTable.getName(atomicNumber);
        document.getElementById("atomicNumberBox").innerHTML=atomicNumber;
        document.getElementById("classBox").innerHTML=PeriodicTable.classOf(atomicNumber);
        document.getElementById("neutronEstimateBox").innerHTML=PeriodicTable.mostLikelyNeutronCount(atomicNumber);
        document.getElementById("chargeBox").innerHTML=MainAtom.getCharge();
        document.getElementById("weightBox").innerHTML=MainAtom.getAtomicWeightRounded()+" AMU";

        document.getElementById("protonCountBox").innerHTML=MainAtom.getProtons();
        document.getElementById("neutronCountBox").innerHTML=MainAtom.getNeutrons();
        document.getElementById("electronCountBox").innerHTML=MainAtom.getElectrons();
    },
    show:function(groupOfSVGElements) {
        document.getElementById("centerSquarePane").appendChild(groupOfSVGElements);
    },
    setDefaultStrokeWidth:function(width) {
        document.getElementById("centerSquarePane").setAttribute("stroke-width",width);
    },
    sizeBinding:new Bindable(0),
    GroupRotationAnimator:class{
        group;
        durationPerRotation;
        startTime;
        constructor(particleGroup,durationPerRotation) {
            this.particleGroup=particleGroup;
            this.durationPerRotation=durationPerRotation;
            this.startTime=performance.now();
            requestAnimationFrame(() => this.updateFrameToNow())
        }
        updateFrameToNow() {
            this.ensureRotation(this.getRotationIShouldBeAt());
            requestAnimationFrame(() => this.updateFrameToNow())
        }
        ensureRotation(rotation) {
            this.particleGroup.setAttribute("transform","rotate("+rotation+")")
        }
        getTimePast() {
            return performance.now()-this.startTime;
        }
        getPercentageOfRotation() {
            return this.getTimePast()/this.durationPerRotation;
        }
        getRotationIShouldBeAt() {
            // console.log(this.getPercentageOfRotation());
            return ((this.getPercentageOfRotation()%1)*360);
        }
    }
}