document.getElementById("saveBtn").addEventListener("click",function() {SaveLoad.save()});
document.getElementById("manualDefinitionBtn").addEventListener("click",function() {
    MainAtom.setMainAtom(document.getElementById("protonBox").value,document.getElementById("neutronBox").value,document.getElementById("electronBox").value)
    DisplayedAtom.copyMainAtom();
});
document.getElementById("numberDefinitionBtn").addEventListener("click",function() {
    MainAtom.setMainAtomByAtomicNumber(document.getElementById("numberBox").value);
    DisplayedAtom.copyMainAtom();
});
document.getElementById("addProtonBtn").addEventListener("click",function() {new Radiation("p",false)});
document.getElementById("remProtonBtn").addEventListener("click",function() {new Radiation("p",true)});
document.getElementById("addNeutronBtn").addEventListener("click",function() {new Radiation("n",false)});
document.getElementById("remNeutronBtn").addEventListener("click",function() {new Radiation("n",true)});
document.getElementById("addElectronBtn").addEventListener("click",function() {new Radiation("e",false)});
document.getElementById("remElectronBtn").addEventListener("click",function() {new Radiation("e",true)});
document.getElementById("alphaEmitBtn").addEventListener("click",function() {new Radiation("a",true)});
document.getElementById("alphaAbsBtn").addEventListener("click",function() {new Radiation("a",false)});
document.getElementById("betaEmitBtn").addEventListener("click",function() {new Radiation("b",true)});
document.getElementById("betaAbsBtn").addEventListener("click",function() {new Radiation("b",false)});
document.getElementById("gammaEmitBtn").addEventListener("click",function() {new Radiation("g",true)});
document.getElementById("gammaAbsBtn").addEventListener("click",function() {new Radiation("g",false)});

dropZone = document.getElementById("dropZone");
dropZone.addEventListener('dragover', (event) => {
  event.preventDefault();
  dropZone.style.backgroundColor = '#ccc'; // Change background on hover
});
dropZone.addEventListener('mouseleave', () => {
    dropZone.style.backgroundColor = ''; // Reset background on leave
});
dropZone.addEventListener('dragleave', () => {
    dropZone.style.backgroundColor = ''; // Reset background on leave
});
dropZone.addEventListener('click', () => {
    document.getElementById("clickFileInput").click(); //do a normal file input behavior when clicked
});  
dropZone.addEventListener('drop', (event) => {
    SaveLoad.load(event.dataTransfer.files[0]);
    event.preventDefault();
});
document.getElementById("clickFileInput").addEventListener('change', (event) => {
    SaveLoad.load(event.target.files[0]);
    event.preventDefault();
});