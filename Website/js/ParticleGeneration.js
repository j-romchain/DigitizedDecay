ParticleGeneration = {
    hadronRadius:new Bindable(100/10),
    packingStageSize:5.0,
    signToNumber: function(typeSign) {
        return ((typeSign=="p") ? 1 :
                    ((typeSign=="n") ? 2 :
                            ((typeSign=="e") ? 3 :
                                    ((typeSign=="a") ? 4 :
                                            ((typeSign=="b") ? 5 :
                                                ((typeSign=="g") ? 6 :
                                                        0))))));
    },
    assembleNucleus: function(neutronCount, protonCount, neucleusPane, nucleusRadiusBindable) {
        parts = this.customArrange(neutronCount+protonCount);
        this.tidyUp(parts);
        this.addCirclesAndBindPositions(parts,neucleusPane,nucleusRadiusBindable);
        this.assignNeutronProton(parts,neutronCount,protonCount);
    },
    customArrange: function(packerCount) {
        packers = new Array(packerCount);
        if (packerCount >= 1) {
            layer = 0;
            packersOnThisLayer  = 1;
            layerRadius = 0;
            var layerCircumference=0;
            totalPackersThisLayer = 1;
            packersLeftForThisLayer = 1;
            for (i = 0; i < packers.length; i++) {
                packer = document.createElementNS("http://www.w3.org/2000/svg","circle");//initialize this packer
                packer.setAttribute("r",this.packingStageSize);
                packers[i] = packer;//add it to the list
                //calculate the angle for this packer to be at, and thus it's position
                angle = Math.PI*2*packersLeftForThisLayer / totalPackersThisLayer;//and then the angle between them...
                x = layerRadius * Math.sin(angle);
                y = layerRadius * Math.cos(angle);
                packer.setAttribute("cx",x);
                packer.setAttribute("cy",y);
                // console.log("angle"+angle)
                // console.log("x"+x)
                // console.log("y"+y)
                packersLeftForThisLayer--;//if this is the last packer in this layer, update vars for the next layer
                if (packersLeftForThisLayer <= 0) {
                    layer++;//increment the layer and the radius and thus circunference
                    layerRadius = 2 * this.packingStageSize * layer;
                    layerCircumference = 2 * Math.PI * (layerRadius);
                    maxPackersThisLayer = Math.floor(layerCircumference / (this.packingStageSize * 2))//and then the max packers that can fit and how many are left to be fitted
                    packersLeftTotal = packers.length-(i+1)
                    totalPackersThisLayer = Math.min(packersLeftTotal,maxPackersThisLayer);//and set the packers for this layer to whichever is smaller
                    packersLeftForThisLayer = totalPackersThisLayer
                }
            }
        }
        return packers;
    },
    tidyUp:function(hadronList) {
        changeAmount = 0.001;
        for (i = 0; i < hadronList.length; i++) {
            hadron =hadronList[i];
            if (hadron!=hadronList[0]) {
                limit = this.packingStageSize * 3;
                hcx=parseFloat(hadron.getAttribute("cx"));
                hcy=parseFloat(hadron.getAttribute("cy"));
                while ((!this.circlesOverlap(hadronList, hadron)) && (limit >= 0)) {
                    centerXNegative = hcx / Math.abs(hcx);
                    centerYNegative = hcy / Math.abs(hcy);
                    limit--;
                    newX = Math.abs(hcx);
                    newY = Math.abs(hcy);

                    angle = Math.atan(newX/newY);
                    newX -= changeAmount*Math.sin(angle);
                    newY -= changeAmount*Math.cos(angle);

                    hadron.setAttribute("cx",centerXNegative * newX);
                    hadron.setAttribute("cy",centerYNegative * newY);
                    hcx=parseFloat(hadron.getAttribute("cx"));
                    hcy=parseFloat(hadron.getAttribute("cy"));
                }
                centerXNegative = hcx / Math.abs(hcx);
                centerYNegative = hcy / Math.abs(hcy);
                newX = Math.abs(hcx);
                newY = Math.abs(hcy);

                angle = Math.atan(newX/newY);
                newX += changeAmount*Math.sin(angle);
                newY += changeAmount*Math.cos(angle);

                hadron.setAttribute("cx",centerXNegative*newX);
                hadron.setAttribute("cy",centerYNegative*newY);
            }
        }
    },
    circlesOverlap:function(circles, main) {
        for (i = 0; i < circles.length; i++) {
            circle=circles[i]
            if (!circle == main) {
                mcx = parseFloat(main.getAttribute("cx"))
                mcy = parseFloat(main.getAttribute("cy"))
                mr = parseFloat(main.getAttribute("r"))
                ccx = parseFloat(circle.getAttribute("cx"))
                ccy = parseFloat(circle.getAttribute("cy"))
                cr = parseFloat(circle.getAttribute("r"))
                distance = Math.sqrt(Math.pow(mcx - ccx, 2) + Math.pow(mcy - ccy, 2));
                if (distance < mr + cr) {
                    return true;
                }
            }
        }
        return false;
    },
    assignNeutronProton:function(hadronList, neutronCount, protonCount) {
        total = hadronList.length;
        for (let i = 0; i < hadronList.length; i++) {
            hadron=hadronList[i]
            randomNumberForIfProton = (Math.random()*total);
            if (randomNumberForIfProton<protonCount) {
                this.makeProton(hadron);
                protonCount-=1;
            } else //noinspection StatementWithEmptyBody
                if (randomNumberForIfProton<neutronCount+protonCount) {
                this.makeNeutron(hadron);
                neutronCount-=1;
            } else {
                //leave it generic
            }
            total-=1;
        }
    },
    addCirclesAndBindPositions:function(circles, squarePaneToBindTo,nucleusRadiusBindable) {
        maxDistance = this.furthestPointFromCenter(circles);
        scaleFactor = new Bindable(1);
        nucleusRadiusBindable.bind({
            setValue:function(value) {
                // console.log("rad:"+value)
                // console.log("max:"+maxDistance)
                // console.log("scale:"+(value/maxDistance))
                scaleFactor.setValue(value/maxDistance)
            }
        });
        scaleFactor.bind({
            setValue:function(value) {
                // console.log("hadronRad:"+value*ParticleGeneration.packingStageSize)
                ParticleGeneration.hadronRadius.setValue(value*ParticleGeneration.packingStageSize)
            }
        });

        for (let i = 0; i < circles.length; i++) {
            circle=circles[i];
            x = parseFloat(circle.getAttribute("cx"));
            y = parseFloat(circle.getAttribute("cy"));
            scaleFactor.bind({
                setValue:function(value){
                    circle.setAttribute("cx",value*x)
                    circle.setAttribute("cy",value*y)
                }
            })
            squarePaneToBindTo.appendChild(circle);
        }
    },
    furthestPointFromCenter:function(circles) {
        maxDistance = 0.0;
        // console.log("hi")
        for (let i = 0; i < circles.length; i++) {
            c=circles[i];
            // console.log("circ")
            cx = parseFloat(c.getAttribute("cx"));
            cy = parseFloat(c.getAttribute("cy"));
            cr = parseFloat(c.getAttribute("r"));
            // console.log(cx)
            distanceFromCenter = Math.sqrt(Math.pow(cx, 2) + Math.pow(cy, 2));
            distanceToEdge = distanceFromCenter + cr;
            // console.log(distanceFromCenter+","+cr+","+distanceToEdge)
            if (distanceToEdge > maxDistance) {
                maxDistance = distanceToEdge;
            }
        }
        return maxDistance;
    },
    genProton:function() {
       return this.makeProton(document.createElementNS("http://www.w3.org/2000/svg",'circle'));
    },
    genNeutron:function() {
       return this.makeNeutron(document.createElementNS("http://www.w3.org/2000/svg",'circle'));
    },
    genElectron:function() {
       return this.makeElectron(document.createElementNS("http://www.w3.org/2000/svg",'circle'));
    },
    makeProton:function(toBecomeProton) {
        return this.configureTron(toBecomeProton,"black","pink",1);
    },
    makeNeutron:function(toBecomeNeutron) {
        return this.configureTron(toBecomeNeutron,"black","lightYellow",1);
    },
    makeElectron:function(toBecomeElectron) {
        return this.configureTron(toBecomeElectron,"black","lightBlue",0.2);
    },
    configureTron:function(circle,border,fill,scaleToHadron) {
        circle.setAttribute("stroke",border);
        circle.setAttribute("fill",fill);
        ParticleGeneration.hadronRadius.bind({
            setValue:function(value) {
                // console.log(value)
                // console.log(scaleToHadron)
                circle.setAttribute("r",value*scaleToHadron)
            }
        });
        circle.setAttribute("style","display: block;");
        return circle;
    }

}
ParticleGeneration.hadronRadius.bind({
    setValue:function(value){
        Display.setDefaultStrokeWidth(value/10)
    }
})