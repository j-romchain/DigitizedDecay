DisplayedAtom = {
    protonCount: 1,
    neutronCount: 1,
    electronCount: 1,
    copyMainAtom: function(){
        this.setProtons(MainAtom.getProtons());
        this.setNeutrons(MainAtom.getNeutrons());
        this.setElectrons(MainAtom.getElectrons());
    },
    setProtons:function(protons) {
        if (protons<0) {
            Error.negativeParticleError();
            return;
        }
        this.protonCount = protons;
        this.display();
    },
    addProtons:function(protonsToAdd) {
        this.setProtons(this.protonCount+protonsToAdd);
    },
    setNeutrons:function(neutrons) {
        if (neutrons<0) {
            Error.negativeParticleError();
            return;
        }
        this.neutronCount = neutrons;
        this.display();
    },
    addNeutrons:function(neutronsToAdd) {
        this.setNeutrons(this.neutronCount+neutronsToAdd);
    },
    setElectrons:function(electrons) {
        if (electrons<0) {
            Error.negativeParticleError();
            return;
        }
        this.electronCount = electrons;
        this.display();
    },
    addElectrons:function(electronsToAdd) {
        this.setElectrons(this.electronCount+electronsToAdd);
    },
    radiation:function(type,emitting) {
        switch (type) {
            case 1:if (emitting) {this.emitProton();  } else { this.absorbProton();  } break;
            case 2:if (emitting) {this.emitNeutron(); } else { this.absorbNeutron(); } break;
            case 3:if (emitting) {this.emitElectron();} else { this.absorbElectron();} break;
            case 4:if (emitting) {this.emitAlpha();   } else { this.unEmitAlpha();   } break;
            case 5:if (emitting) {this.emitBeta();    } else { this.unEmitBeta();    } break;
            case 6:if (emitting) {this.emitGamma();   } else { this.unEmitGamma();   } break;
        }
        this.display();
    },
    unEmitAlpha:function() {
        this.addProtons(2);
        this.addNeutrons(2);
    },
    emitAlpha:function() {
        this.addProtons(-2);
        this.addNeutrons(-2);
    },
    unEmitBeta:function() {
        this.addProtons(-1);
        this.addNeutrons(1);
    },
    emitBeta:function() {
        this.addProtons(1);
        this.addNeutrons(-1);
    },
    unEmitGamma:function() {
    },
    emitGamma:function() {
    },
    absorbProton:function() {
        this.addProtons(1);
    },
    emitProton:function() {
        this.addProtons(-1);
    },
    absorbNeutron:function() {
        this.addNeutrons(1);
    },
    emitNeutron:function() {
        this.addNeutrons(-1);
    },
    absorbElectron:function() {
        this.addElectrons(1);
    },
    emitElectron:function() {
        this.addElectrons(-1);
    },
    init: false,
    display:function() {
        if (!this.init) {
            //init the panes
            this.mainPane = document.createElementNS("http://www.w3.org/2000/svg","g");
            this.mainPane.setAttribute("stroke","black")
            this.nucleusPane = document.createElementNS("http://www.w3.org/2000/svg","g");
            this.mainPane.appendChild(this.nucleusPane)
            this.electronPane = document.createElementNS("http://www.w3.org/2000/svg","g");
            this.mainPane.appendChild(this.electronPane)
            //and set the nucleusPane spinning (the electronSubPanes will have their own spinners added as they are made)
            var nucleusRotator = new Display.GroupRotationAnimator(this.nucleusPane,((1/((Math.random()*4)+1)*1000*60)));
            Display.show(this.mainPane); 
            this.init=true
        }
        // console.log("p"+this.protonCount+"n"+this.neutronCount+"e"+this.electronCount)
        Display.updateInfo();
        // mainPane.setAttribute("id","remmingDisplayedAtom")
        this.generateBohrModel()
    },
    nucleusPane: document.createElementNS("http://www.w3.org/2000/svg","g"),
    electronPane: document.createElementNS("http://www.w3.org/2000/svg","g"),
    electronSubPanes: [],
    electronSubPaneSpinners: [],
    mainPane: document.createElementNS("http://www.w3.org/2000/svg","g"),
    generateBohrModel:function() {
        // Calculate the radius difference between each shell based on the number of shells needed and the width of the Pane
        var layerThickness = new Bindable(50/(PeriodicTable.likelyNumShells(this.electronCount) + 2))
        var nucleusRadius = new Bindable(0);layerThickness.bind(nucleusRadius);
        // wipe the nucleus and electron panes individually, so as to not disrupt the child higharchy
        document.createElementNS("http://www.w3.org/2000/svg","g").chil
        while (this.nucleusPane.children.length>0) {
            element = this.nucleusPane.children[0];
            this.nucleusPane.removeChild(element);
        }
        while (this.electronPane.children.length>0) {
            ring = this.electronPane.children[0];
            while (ring.children.length>0) {
                element = ring.children[0];
                ring.removeChild(element);
            }
            this.electronPane.removeChild(ring);
        }
        //nucleus
        {
            // Create a Circle for the back of the nucleus and add it to the Pane
            var centerCircle = document.createElementNS("http://www.w3.org/2000/svg","circle");
            centerCircle.setAttribute("fill","grey")
            centerCircle.setAttribute("stroke","black")
            this.nucleusPane.appendChild(centerCircle);
            nucleusRadius.bind({
                setValue:function(value) {
                    centerCircle.setAttribute("r",value);
                }
            })
            centerCircle.setAttribute("cx",0);
            centerCircle.setAttribute("cy",0);

            // Calculate the size of neutrons and protons based on the amount that must fit in the nucleus, and the size of the nucleus
            ParticleGeneration.assembleNucleus(this.neutronCount, this.protonCount, this.nucleusPane,nucleusRadius);
        }
        //electrons
        {
            var electronsLeft = this.electronCount;
            // Create a Pane and Circle for each shell and add it to the main electron Pane, and then make as many electrons as is expected in that shell
            for (var i = 0; i < PeriodicTable.likelyNumShells(this.electronCount); i++) {
                if (this.electronSubPanes.length<=i) {
                    this.electronSubPanes.push(document.createElementNS("http://www.w3.org/2000/svg","g"))
                    this.electronSubPaneSpinners.push(new Display.GroupRotationAnimator(this.electronSubPanes[i],((1/((Math.random()*4)+1)*1000*60))));
                }
                var electronRing = this.electronSubPanes[i];
                this.electronPane.appendChild(electronRing);
                var thisRingRadius = new Bindable();
                layerThickness.bind({
                    setValue:function(value) {
                        thisRingRadius.setValue(value*(i+2));
                    }
                });
                var ring = document.createElementNS("http://www.w3.org/2000/svg","circle");
                electronRing.appendChild(ring);
                ring.setAttribute("fill","none")
                ring.setAttribute("stroke","black")
                thisRingRadius.bind({
                    setValue:function(value) {
                        ring.setAttribute("r",value);
                    }
                })
                var electronsInThisShell = Math.min(electronsLeft, PeriodicTable.electronsInShell(i + 1));
                var angleBetweenElectronsInThisShell = (2*Math.PI/electronsInThisShell);
                electronsLeft -= electronsInThisShell;
                for (var j = 0; j < electronsInThisShell; j++) {
                    var angle = angleBetweenElectronsInThisShell * (j + 1);
                    var electron = document.createElementNS("http://www.w3.org/2000/svg","circle");
                    electronRing.appendChild(electron);
                    ParticleGeneration.makeElectron(electron);
                    thisRingRadius.bind({
                        setValue:function(value) {
                            electron.setAttribute("cx",value*Math.cos(angle));
                            electron.setAttribute("cy",value*Math.sin(angle));
                        }
                    })
                }
            }
        }
        return this.mainPane;
    }
}

DisplayedAtom.copyMainAtom();